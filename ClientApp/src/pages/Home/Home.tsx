import { FC } from 'react'
import styles from './Home.module.scss'

interface IHomeProps {}

const Home: FC<IHomeProps> = () => {
  return <div>Home</div>
}

export default Home
