import { FC } from 'react'
import Home from './pages/Home/Home'
import Layout from './hoc/Layout/Layout'

interface IAppProps {}

const App: FC<IAppProps> = () => {
  return (
    <Layout>
      <Home />
    </Layout>
  )
}

export default App
